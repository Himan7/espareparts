﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ESpareParts.Web.Startup))]
namespace ESpareParts.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
